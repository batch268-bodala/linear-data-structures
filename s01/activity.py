numbers = [1, 2, 3, 4, 5]
meals = ['spam', 'Spam', 'SPAM!']

print(f'list of numbers: {numbers} \n')
print(f'list of meals: {meals} \n')

messages = ["Hi!"]*4
print(f'Result of repeat operator: {messages} \n')

print("List literals and operations using 'numbers' list:")
print(f'Length of the list: {len(numbers)}')

last_item = numbers.pop()
print(f'The last item was removed from list: {last_item}')

print(f'The list in reversed order: {numbers[::-1]} \n')

print("List literals an doperations using 'meals' list:")
print(f"Using indexing operation: {meals[2]}")
print(f"Using negative indexing operation: {meals[-2]}")

meals[1] = "eggs"
print(f"Modified list: {meals}")

meals.append('bacon')
print(f"Added a new item in the list: {meals}")
meals.sort()
print(f"Modified list after sort method: {meals} \n")

recipe = {
	"eggs" : 3
}
print(f'Created recipe dictionary: \nrecipe = {recipe} \n')

recipe["spam"] = 2
recipe["ham"] = 1
recipe["brunch"] = "Bacon"
print(f'Modified recipe dictionary: \nrecipe = {recipe} \n')

recipe["ham"] = ["grill", "bake", "fry"]
print(f"Updated the 'ham' key value: \nrecipe = {recipe} \n")

del recipe["eggs"]
print(f"Modified recipe after deleting 'eggs' key: \nrecipe = {recipe} \n")

bob = {
	"name": {
		"first": "Bob",
		"last": "Smith"
	},
	"age": 42,
	"job": ["software", "writing"],
	"pay": (40000, 50000)
}

print(f"Given 'bob' dictionary:\nbob = {bob} \n")
print(f"Accessing the value of 'name' key: {bob['name']}")
print(f"Accessing the value of 'last' key: {bob['name']['last']}")
print(f"Accessing the second value of 'pay' key: {bob['pay'][1]} \n")


numeric = ('twelve', 3.0, [11, 22, 33])
print(f"Given 'numeric' tuple: \nnumeric = {numeric} \n")
print(f"Accessing a tuple item: {numeric[1]}")
print(f"Accessing a tuple item: {numeric[2][1]}")