class Node:
	def __init__ (self, color):
		self.color = color
		self.next = None

class ColorLinkedList:
	def __init__(self):
		self.head = None

	def append(self, color):
		new_node = Node(color)
		if not self.head:
			self.head = new_node
		else:
			current = self.head
			while current.next:
				current = current.next
			current.next = new_node

	def print(self):
		current = self.head
		while current:
			print(current.color, end="\n")
			current = current.next
		print('\n')

	def prepend(self, color):
		new_node = Node(color)
		new_node.next = self.head
		self.head = new_node

	def delete(self, color):
		if not self.head:
			return

		if self.head.color == color:
			self.head = self.head.next
			return

		current = self.head
		while current.next:
			if current.next.color == color:
				current.next = current.next.next
				return

			current = current.next

	def search(self, color):
		current = self.head
		while current:
			if current.color == color:
				return 'Yes'
			current = current.next
		return 'No'

	def count(self):
		count = 0
		current = self.head
		while current:
			count += 1
			current = current.next
		return count

llist = ColorLinkedList()
llist.append("Brown")
llist.append("Green")
llist.append("Blue")
llist.append("Gray")
llist.append("Black")

print("Original nodes of the linked list:")
llist.print()


print('Insert the color "Purple" at the front of the linked list:')
llist.prepend("Purple")
# Print the updated linked list
llist.print()


print('Delete the color "Black" from the linked list:')
llist.delete("Black")
llist.print()


print("Does the color 'Gray' exist in the linked list?\n", llist.search("Gray"), '\n')
print("Does the color 'Black' exist in the linked list?\n", llist.search("Black"), '\n')


print("Total number of nodes in the linked list:\n", llist.count())