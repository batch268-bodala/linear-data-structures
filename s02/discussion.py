# [SECTION] Linked List

# Represents a node within a linked list
class Node:
	def __init__(self, data):
		self.data = data # It has its own data/value
		self.next = None # It also has a pointer to the next node

# Represents a linked list
class LinkedList:
	# Initializes the head of the linked list which will initially be none.
	def __init__(self):
		self.head = None

	# Prepend function is responsible for initializing the head of the linked list
	def prepend(self, data):
		new_node = Node(data) # Utilizes the Node class to create a new node
		new_node.next = self.head # Sets the pointer to the head of the current node
		self.head = new_node # Sets the head of the current node to the new node

	# Append function is responsible for attaching new nodes to the existing head node in the linked list
	def append(self, data):
		new_node = Node(data)

		# Checks if there is already a head in the linked list, if there is none, it will simple assign the new node as the head
		if self.head is None:
			self.head = new_node 
			return

		# Assign the head of the linked list as the last node of the linked list
		last_node = self.head 

		while last_node.next:
			# As long as the last_node.next has any data
			last_node = last_node.next # last_node will be set as the value of the next

		# The new_node will now be set at last_node.next
		last_node.next = new_node

	def print_list(self):
		current_node = self.head

		while current_node:
			print(current_node.data) # While the head node has data, it will print the head node and re-assign the value of the current_node
			current_node = current_node.next

	def delete_node(self, key):
		# Initializes the current node as the head of the linked list
		current_node = self.head

		# If the value of the current_node/current_node.data is equal to the key that was provided as an argument, then reassign the head of the linked list to the next node and re-assign the value of the current node to none.
		if current_node and current_node.data == key:
			self.head = current_node.next
			current_node = None
			return # The return statement will end the delete_node function

		# Initialize our previous variable to None
		previous = None 

		# While the current node is empty and the node data value is not equal to the user input value
		while current_node and current_node.data != key:
			previous = current_node # Reassign the previous variable to hold the current node value 
			current_node = current_node.next # Reassign the current node and move it to the next node

		# If the current node is empty then exit the function
		if current_node is None:
			return 

		# Previous next pointer now points to the next node disregarding the node that will be deleted
		previous.next = current_node.next

		current_node = None

# Initializing the linked list
linked_list = LinkedList()

# Using the prepend function to assign new data to a new node in the linked list
linked_list.prepend('A')

# Append new values to the linked list
linked_list.append('B')
linked_list.append('C')
linked_list.append('D')

# Deleting the 'A' node from the linked list
linked_list.delete_node('A')

# To print the current linked list
linked_list.print_list()
