from numpy import *

rows = int(input("Enter the number of rows -> "))
columns = int(input("Enter the number of columns -> "))

matrixOne = zeros((rows, columns), dtype=int)
matrixTwo = zeros((rows, columns), dtype=int)

print("Input the values for First matrix")
for i in range(rows):
    for j in range(columns):
        matrixOne[i][j] = input(f"Enter element at matrixOne[{i + 1}][{j + 1}]= ")

print("Input the values for First matrix")
for i in range(rows):
    for j in range(columns):
        matrixTwo[i][j] = input(f"Enter element at matrixTwo[{i + 1}][{j + 1}]= ")

print("*******Display Matrix1*******")
for i in range(rows):
    for j in range(columns):
        print(matrixOne[i][j], "       ", end="")
    print("\n")

print("*******Display Matrix1*******")
for i in range(rows):
    for j in range(columns):
        print(matrixTwo[i][j], "       ", end="")
    print("\n")

print('Sum of Two matrices')
matrixSum = add(matrixOne, matrixTwo)
for i in range(rows):
    for j in range(columns):
        print(matrixSum[i][j], "       ", end="")
    print("\n")
