from numpy import *

# array() function allows us to create a matrix using numPy
box = array([
	[1, 2, 3],
	[4, 5, 6],
	[7, 8, 9],
	[11, 12, 10]
])

print(box)


# [SECTION] Matrix Methods

# 1. .ndim - Give us the number of dimensions in the array
print(box.ndim)

# 2. .shape - Gives the number of columns and rows
print(box.shape)

# 3. .size - Gives the size of the array
print(box.size)

# 4. flatten() - Merges the elements into one array making it 1 dimensional
flat_box = box.flatten()
print(flat_box)

# 5. reshape() - Changes the dimensions of the array
print(flat_box.reshape(2, 6))

# 6. diagonal() - Finds the elements in a diagonal position
diagonal_matrix = array([
	[0, 1, 2],
	[3, 4, 5],
	[6, 7, 8]
])

print(diagonal(diagonal_matrix))


# [SECTION] Manipulating the Matrix

# 1. Indexing
sched = array([
	['Mon', 5, 10, 2],
	['Tue', 4, 3, 1],
	['Wed', 7, 8, 9]
])

print(sched[1][2])

# 2. Appending and Inserting
new_sched = append(sched, [['Thu', 23, 12, 13]], 0)
print(new_sched)
print('------------------')

# 3. Delete 
new_sched = delete(sched, [2], 0)
print(new_sched)

# 4. Operations - matrices also has the ability to have mathematical operations done on them
matrix_one = array([
	[5, 10],
	[5, 5]
])

matrix_two = array([
	[5, 10],
	[5, 5]
])

# Add
print(add(matrix_one, matrix_two))

# Subtract
print(subtract(matrix_one, matrix_two))

# Divide
print(divide(matrix_one, matrix_two))

# Multiply
print(multiply(matrix_one, matrix_two))