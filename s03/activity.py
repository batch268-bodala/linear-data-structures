class PriorityLane:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        self.items.append(item)

    def dequeue(self):
        if not self.is_empty():
            return self.items.pop(0)
        else:
            raise IndexError("Queue is empty")

    def is_empty(self):
        return not self.items

    def peek(self):
        if self.items:
            return self.items[0]
        else:
            raise IndexError("Queue is empty")

    def size(self):
        return len(self.items)

    def print_queue(self):
        print(self.items)

security = PriorityLane()

security.enqueue("senior")
security.enqueue("pregnant")
security.enqueue("PWD")
security.enqueue("normal person")

print("Original queue list items:")
security.print_queue()

print("\nIs security list empty?", security.is_empty(), '\n')

print("The current size of the security list:", security.size(), '\n')

print("Peek of the new list queue item:", security.peek(), '\n')

print("The item is removed from the security list:", security.dequeue(), '\n')